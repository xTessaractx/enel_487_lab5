/**
Filename: a2d.c
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab5
*/

#include "main.h"
#include <cmath>

//This is the global variable that holds the ADC output.
static int global_adc_output = 1;

/**
		The a2d_init functoin initializes the registers
		necissary for the ADC to properly function.
		It takes no parameters and returns nothing.
*/
void a2d_init()
{
	 const uint32_t CHANNEL_10 = 0x0000000A;

	 //Enable peripheral clocks for ADC1 and the GPIOC.
	*regRCC_APB2ENR |= 0x00000210;
 
	//Set Channel 10 to be an analog inputs.
	*regGPIOC_CRL |= 0x000000004;
	 
	//Set the SQR3 register equal to zero to clear the entire register.
	*regADC1_SQR3 |= 0x0;	 	 
	
 	//Sets the SQR3 register so that channel 10 can be used as an input for the ADC
	*regADC1_SQR3 = CHANNEL_10;
	 
	 //Set the External Trigger and EXTSEL bits to all high
	*regADC1_CR2 |= 0x001E0000;

	//Set the sample rate to 55.5 Cycles.
	*regADC1_SMPR1 = 0x00000005;
	
	//Enable Scan mode and EOC Interupt Enable
	*regADC1_CR1 |= 0x00000120;

	//Turn the ADC bit on
	*regADC1_CR2 |= 0x00000001;

 	//Turn on Reset claibration bit
	*regADC1_CR2 |= 0x00000008;

	//Wait for the RST_CAL bit to go low.
	while ((*regADC1_CR2 & 0x00000008) == 0x00000008)
	{
	}

 	//Turn on Claibration bit
	*regADC1_CR2 |= 0x00000004;

	//Wait for the RST_CAL bit to go low.
	while ((*regADC1_CR2 & 0x00000004) == 0x00000004) //Checking if the EOC bit of ADC1->SR is high.
	{
	}
	
	*regNVIC_ISER0 = 0x00040000;
	
	return;
}

/**
		The ADC1_2_IRQHandler functoin is called when the ADC interrupt occurs.
		It obtains the value stored in the *regADC1_DR register and stores
		it in the global variable global_adc_output.
		It takes no parameters and returns nothing.
*/
void ADC1_2_IRQHandler(void)
{
	//Read the data from the ADC data register
	global_adc_output = *regADC1_DR;
}

/**
		The get_global_adc_output functoin returns the value stored
		in the global variable global_adc_output.
		It allows for global_adc_output to be used elsewhere in the program.
		It takes no parameters and returns an integer (the value stored in global_adc_output).
*/
int get_global_adc_output()
{
	return global_adc_output;
}
